﻿namespace HOMEWork
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbEng = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbEng = new System.Windows.Forms.TextBox();
            this.tbMath = new System.Windows.Forms.TextBox();
            this.tbChi = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnSummary = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnAddMulti = new System.Windows.Forms.Button();
            this.btnAddRandom = new System.Windows.Forms.Button();
            this.test = new System.Windows.Forms.Button();
            this.view = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.ForeColor = System.Drawing.Color.DarkRed;
            this.label1.Location = new System.Drawing.Point(223, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "學生成績統計";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lbName.Location = new System.Drawing.Point(19, 51);
            this.lbName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(51, 18);
            this.lbName.TabIndex = 1;
            this.lbName.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(19, 166);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Chi:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(19, 129);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Math";
            // 
            // lbEng
            // 
            this.lbEng.AutoSize = true;
            this.lbEng.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lbEng.Location = new System.Drawing.Point(19, 88);
            this.lbEng.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbEng.Name = "lbEng";
            this.lbEng.Size = new System.Drawing.Size(37, 18);
            this.lbEng.TabIndex = 4;
            this.lbEng.Text = "Eng:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(73, 49);
            this.tbName.Margin = new System.Windows.Forms.Padding(2);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(68, 22);
            this.tbName.TabIndex = 5;
            // 
            // tbEng
            // 
            this.tbEng.Location = new System.Drawing.Point(73, 85);
            this.tbEng.Margin = new System.Windows.Forms.Padding(2);
            this.tbEng.Name = "tbEng";
            this.tbEng.Size = new System.Drawing.Size(68, 22);
            this.tbEng.TabIndex = 6;
            // 
            // tbMath
            // 
            this.tbMath.Location = new System.Drawing.Point(73, 126);
            this.tbMath.Margin = new System.Windows.Forms.Padding(2);
            this.tbMath.Name = "tbMath";
            this.tbMath.Size = new System.Drawing.Size(68, 22);
            this.tbMath.TabIndex = 7;
            // 
            // tbChi
            // 
            this.tbChi.Location = new System.Drawing.Point(73, 163);
            this.tbChi.Margin = new System.Windows.Forms.Padding(2);
            this.tbChi.Name = "tbChi";
            this.tbChi.Size = new System.Drawing.Size(68, 22);
            this.tbChi.TabIndex = 8;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnAdd.Location = new System.Drawing.Point(23, 209);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(165, 22);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "Add Student";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSummary
            // 
            this.btnSummary.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSummary.Location = new System.Drawing.Point(23, 285);
            this.btnSummary.Margin = new System.Windows.Forms.Padding(2);
            this.btnSummary.Name = "btnSummary";
            this.btnSummary.Size = new System.Drawing.Size(165, 22);
            this.btnSummary.TabIndex = 10;
            this.btnSummary.Text = "Summary";
            this.btnSummary.UseVisualStyleBackColor = true;
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnReset.Location = new System.Drawing.Point(23, 325);
            this.btnReset.Margin = new System.Windows.Forms.Padding(2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(165, 22);
            this.btnReset.TabIndex = 11;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            // 
            // btnAddMulti
            // 
            this.btnAddMulti.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnAddMulti.Location = new System.Drawing.Point(23, 365);
            this.btnAddMulti.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddMulti.Name = "btnAddMulti";
            this.btnAddMulti.Size = new System.Drawing.Size(165, 22);
            this.btnAddMulti.TabIndex = 12;
            this.btnAddMulti.Text = "Add 20 Student";
            this.btnAddMulti.UseVisualStyleBackColor = true;
            // 
            // btnAddRandom
            // 
            this.btnAddRandom.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnAddRandom.Location = new System.Drawing.Point(23, 247);
            this.btnAddRandom.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddRandom.Name = "btnAddRandom";
            this.btnAddRandom.Size = new System.Drawing.Size(165, 22);
            this.btnAddRandom.TabIndex = 13;
            this.btnAddRandom.Text = "Add Student(random)";
            this.btnAddRandom.UseVisualStyleBackColor = true;
            this.btnAddRandom.Click += new System.EventHandler(this.btnAddRandom_Click);
            // 
            // test
            // 
            this.test.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.test.Location = new System.Drawing.Point(665, 39);
            this.test.Margin = new System.Windows.Forms.Padding(2);
            this.test.Name = "test";
            this.test.Size = new System.Drawing.Size(165, 108);
            this.test.TabIndex = 15;
            this.test.Text = "test";
            this.test.UseVisualStyleBackColor = true;
            // 
            // view
            // 
            this.view.Location = new System.Drawing.Point(227, 73);
            this.view.Multiline = true;
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(412, 398);
            this.view.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1304, 719);
            this.Controls.Add(this.view);
            this.Controls.Add(this.test);
            this.Controls.Add(this.btnAddRandom);
            this.Controls.Add(this.btnAddMulti);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSummary);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.tbChi);
            this.Controls.Add(this.tbMath);
            this.Controls.Add(this.tbEng);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lbEng);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbEng;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbEng;
        private System.Windows.Forms.TextBox tbMath;
        private System.Windows.Forms.TextBox tbChi;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnSummary;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnAddMulti;
        private System.Windows.Forms.Button btnAddRandom;
        private System.Windows.Forms.Button test;
        private System.Windows.Forms.TextBox view;
    }
}


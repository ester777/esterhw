﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HOMEWork
{
    public partial class Form1 : Form
    {
        public DataGridView datagridview1 = new DataGridView();
        public BindingSource bindingSource1 = new BindingSource();
        public DataTable dt = new DataTable();

        public Form1()
        {
            InitializeComponent();
            initView();
        }

        /// <summary>
        /// 初始化學生資料
        /// FORM_LOAD時劃出COLUMN+ROW,PRINT()出COLUMN標題
        /// </summary>
        /// 
        internal void initView()
        {

            datagridview1.DataSource = dt;
            DataColumn column;
            DataRow row;

            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "StudentName";
            column.Caption = "StudentName";
            column.AllowDBNull = true;
            column.Unique = false;
            //名稱不能重複, IF用於成績則只能有一人100,ETC.
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "Eng";
            column.Caption = "Eng";
            column.AllowDBNull = true;
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "Math";
            column.Caption = "Math";
            column.AllowDBNull = true;
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "Chinese";
            column.Caption = "Chinese";
            column.AllowDBNull = true;
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "SUM";
            column.Caption = "SUM";
            column.AllowDBNull = true;
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "AVG";
            column.Caption = "AVG";
            column.AllowDBNull = true;
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "MAX";
            column.Caption = "MAX";
            column.AllowDBNull = true;
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "MIN";
            column.Caption = "MIN";
            column.AllowDBNull = true;
            dt.Columns.Add(column);


            row = dt.NewRow();
            row[0] = "StudentName";
            row[1] = "ENG";
            row[2] = "MATH";
            row[3] = "CHINESE";
            row[4] = "SUM";
            row[5] = "AVG";
            row[6] = "MAX";
            row[7] = "MIN";
            dt.Rows.Add(row);

            print();
        }

            /// <summary>
            /// PRINT()各排標題
            /// </summary>
            /// 
         string s;
         private void print()
         {
             foreach (DataRow row in dt.Rows)
             {
                    s = $"{row[0].ToString()}    \n" +
                        $"{row[1].ToString()}    \n" +
                        $"{row[2].ToString()}    \n" +
                        $"{row[3].ToString()}    \n" +
                        $"{row[4].ToString()}    \n" +
                        $"{row[5].ToString()}    \n" +
                        $"{row[6].ToString()}    \n" +
                        $"{row[7].ToString()}    \r\n";

                    view.AppendText(s);
             }
         }


         private void btnAdd_Click(object sender, EventArgs e)
         {
                DataRow row;
                row = dt.NewRow();
                row[0] = tbName.Text;
                row[1] = tbEng.Text;
                row[2] = tbMath.Text;
                row[3] = tbChi.Text;
                dt.Rows.Add(row);
                s = $"{row[0]}             \n" +
                    $"{row[1]}             \n" +
                    $"{row[2]}             \n" +
                    $"{row[3]}             \r\n";

               view.AppendText(s);
        }

        private void btnAddRandom_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            while (dt.Rows.Count > 10)
            {
                int j = r.Next(0, dt.Rows.Count);
                dt.Rows.RemoveAt(j);
            }
            s = $"{row[0]}             \n" +
                  $"{row[1]}             \n" +
                  $"{row[2]}             \n" +
                   $"{row[3]}             \r\n";

            view.AppendText(s);
        }
    }
}
